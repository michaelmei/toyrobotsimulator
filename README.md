# Toy Robot Simulator

The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.  
There are no other obstructions on the table surface.  
The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement that would result in the robot falling from the table must be prevented, however further valid movement commands must still
be allowed.  

# Commands of the following form are valid
PLACE X,Y,F  
MOVE  
LEFT  
RIGHT  
REPORT  

PLACE will put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST.  
The origin (0,0) can be considered to be the SOUTH WEST most corner.  
The first valid command to the robot is a PLACE command, after that, any sequence of commands may be issued, in any order, including another PLACE command. The application should discard all commands in the sequence until a valid PLACE command has been executed.
MOVE will move the toy robot one unit forward in the direction it is currently facing.  
LEFT and RIGHT will rotate the robot 90 degrees in the specified direction without changing the position of the robot.  
REPORT will announce the X,Y and orientation of the robot.  
A robot that is not on the table can choose to ignore the MOVE, LEFT, RIGHT and REPORT commands.  
Provide test data to exercise the application.  

Constraints:  
The toy robot must not fall off the table during movement. This also includes the initial placement of the toy robot.  
Any move that would cause the robot to fall must be ignored.  

# Compile, run unit tests, run main program
go to the project directory  
gradle build  
gradle test  
java -jar build/libs/robotExercise.jar   

# Sample manual tests

**Toy Robot Powered On, Enjoy! Type 'EXIT' to exit**  
**Command specification:**  
**1.   PLACE X,Y,F (where x,y are axises of the table surface x and y and F is the Facing Direction: NORTH, EAST, SOUTH, WEST)**  
**2.   MOVE**  
**3.   LEFT**   
**4.   RIGHT**   
**5.   REPORT** 

PLACE 0,0,NORTH  
MOVE  
REPORT  
**Output: 0,1,NORTH**  
PLACE 0,0,NORTH  
LEFT  
REPORT  
**Output: 0,0,WEST**  
PLACE 1,2,EAST  
MOVE  
MOVE  
LEFT  
MOVE  
REPORT  
**Output: 3,3,NORTH**  
exit  

# Test Cases (see details in the junit test cases in /src/test/java/UnitTests.java)  
1. Command parsing unit tests  
2. Rotation unit tests  
3. Placing robot into invalid position unit tests  
4. Robot moving unit test(predefined path)  
5. Robot moving exhausted test(randomly rotate and move to cover all table surface area)  
