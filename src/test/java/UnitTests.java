import com.macbank.exercise.Robot;
import com.macbank.exercise.TableSurface;
import com.macbank.exercise.util.*;
import junit.framework.TestCase;
import org.junit.Test;

import java.util.Random;

public class UnitTests extends TestCase {

    private TableSurface tableSurface; //5x5 grids, we start with index 0

    @Override
    public void setUp() throws Exception {
        tableSurface = TableSurface.getTableInstance(4,4);
    }

    @Override
    public void tearDown() throws Exception {
        tableSurface = null;
    }


//    > > PLACE X,Y,F
//> > MOVE
//> > LEFT
//> > RIGHT
//> > REPORT
    @Test
    public void testCommandParsing() {
        CommandResult result;

        result = CommandLineParser.parse("PLACE");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE SOUTH");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE 1,SOUTH");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE 1,2,SOUTH");
        assertEquals("command line PLACE", null == result, false);
        assertEquals("command line PLACE", result.getCommand(), Command.PLACE);
        assertEquals("command line PLACE", result.getFacingDirection(), FacingDirection.SOUTH);
        assertEquals("command line PLACE", result.getPosX().intValue(), 1);
        assertEquals("command line PLACE", result.getPosY().intValue(), 2);

        result = CommandLineParser.parse("PLACE 1,2,WEST1");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE 1,as,WEST");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE as,1,WEST");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE 1,2,WEST,ss");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("PLACE 1,2,WEST,1,ss");
        assertEquals("command line PLACE", null == result, true);

        result = CommandLineParser.parse("MOVE");
        assertEquals("command line MOVE", null != result && result.getCommand().equals(Command.MOVE), true);

        result = CommandLineParser.parse("MOVE asdad");
        assertEquals("command line MOVE", null == result, true);

        result = CommandLineParser.parse("LEFT");
        assertEquals("command line LEFT", null != result && result.getCommand().equals(Command.LEFT), true);

        result = CommandLineParser.parse("RIGHT");
        assertEquals("command line LEFT", null != result && result.getCommand().equals(Command.RIGHT), true);

        result = CommandLineParser.parse("REPORT");
        assertEquals("command line LEFT", null != result && result.getCommand().equals(Command.REPORT), true);

        result = CommandLineParser.parse("REPOR");
        assertEquals("command line MOVE", null == result, true);
    }

    @Test
    public void testRotations() {
        assertEquals("EAST rotate left:", DirectionHelper.left(FacingDirection.EAST), FacingDirection.NORTH);
        assertEquals("EAST rotate right:", DirectionHelper.right(FacingDirection.EAST), FacingDirection.SOUTH);
        assertEquals("NORTH rotate left:", DirectionHelper.left(FacingDirection.NORTH), FacingDirection.WEST);
        assertEquals("NORTH rotate right:", DirectionHelper.right(FacingDirection.NORTH), FacingDirection.EAST);
        assertEquals("SOUTH rotate left:", DirectionHelper.left(FacingDirection.SOUTH), FacingDirection.EAST);
        assertEquals("SOUTH rotate right:", DirectionHelper.right(FacingDirection.SOUTH), FacingDirection.WEST);
        assertEquals("WEST rotate left:", DirectionHelper.left(FacingDirection.WEST), FacingDirection.SOUTH);
        assertEquals("WEST rotate right:", DirectionHelper.right(FacingDirection.WEST), FacingDirection.NORTH);
    }

    @Test
    public void testWrongRobotPlacement() {
        Robot robot = new Robot();

        assertEquals("excepted wrong placement:",
                robot.hopOn(tableSurface, -1, -1, FacingDirection.NORTH), false);
        assertEquals("excepted wrong placement:",
                robot.hopOn(tableSurface, 0, 5, FacingDirection.NORTH), false);
        assertEquals("excepted wrong placement:",
                robot.hopOn(tableSurface, 5, 0, FacingDirection.NORTH), false);

        assertEquals("movement failure because of the wrong placement: ",
                robot.move(), false);

        assertEquals("left error because of the wrong placement: ", robot.turnLeft(), false);
        assertEquals("right error because of the wrong placement: ", robot.turnRight(), false);
    }

    @Test
    public void testRobotBoundaryMovement() {
        Robot robot = new Robot();

        assertEquals("excepted right placement:", robot.hopOn(tableSurface, 0, 0, FacingDirection.NORTH), true);
        robot.turnRight(); //EAST
        robot.report();
        assertEquals("Robot turnRight: ", robot.toString().equals("0,0,EAST"), true);

        robot.move(); //(1, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("1,0,EAST"), true);

        robot.move(); //(2, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("2,0,EAST"), true);

        robot.move(); //(3, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,0,EAST"), true);

        robot.move(); //(4, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("4,0,EAST"), true);

        robot.move(); //(4, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("4,0,EAST"), true);

        robot.turnRight(); //SOUTH
        robot.report();
        assertEquals("Robot turnRight: ", robot.toString().equals("4,0,SOUTH"), true);

        robot.move(); //(4, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("4,0,SOUTH"), true);

        robot.turnRight(); //WEST
        robot.report();
        assertEquals("Robot turnRight: ", robot.toString().equals("4,0,WEST"), true);

        robot.move(); //(3, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,0,WEST"), true);

        robot.move(); //(2, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("2,0,WEST"), true);

        robot.move(); //(1, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("1,0,WEST"), true);

        robot.move(); //(0, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("0,0,WEST"), true);

        robot.move(); //(0, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("0,0,WEST"), true);

        robot.turnLeft(); //SOUTH
        robot.report();
        assertEquals("Robot turnLeft: ", robot.toString().equals("0,0,SOUTH"), true);

        robot.turnLeft(); //EAST
        robot.report();
        assertEquals("Robot turnLeft: ", robot.toString().equals("0,0,EAST"), true);

        robot.move(); //(1, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("1,0,EAST"), true);

        robot.move(); //(2, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("2,0,EAST"), true);

        robot.move(); //(3, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,0,EAST"), true);

        robot.turnLeft(); //NORTH
        robot.report();
        assertEquals("Robot turnLeft: ", robot.toString().equals("3,0,NORTH"), true);

        robot.move(); //(3, 1)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,1,NORTH"), true);

        robot.move(); //(3, 2)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,2,NORTH"), true);

        robot.move(); //(3, 3)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,3,NORTH"), true);

        robot.move(); //(3, 4)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,4,NORTH"), true);

        robot.move(); //(3, 4)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,4,NORTH"), true);

        robot.turnRight(); //EAST
        robot.report();
        assertEquals("Robot turnRight: ", robot.toString().equals("3,4,EAST"), true);

        robot.turnRight(); //SOUTH
        robot.report();
        assertEquals("Robot turnRight: ", robot.toString().equals("3,4,SOUTH"), true);

        robot.move(); //(3, 3)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,3,SOUTH"), true);

        robot.move(); //(3, 2)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,2,SOUTH"), true);

        robot.move(); //(3, 1)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,1,SOUTH"), true);

        robot.move(); //(3, 0)
        robot.report();
        assertEquals("Robot move: ", robot.toString().equals("3,0,SOUTH"), true);

        robot.move(); //(3, 0)
        robot.report();
        assertEquals("Robot predefined boundary movement destination: ", robot.toString().equals("3,0,SOUTH"), true);

        robot.getOff();
    }

    @Test
    public void testRobotMoveAroundTableSurfaceWithBoundaryCheckup() {
        final int MAX_X_AXIS = 5, MAX_Y_AXIS = 5;
        boolean[][] mapStatus = new boolean[MAX_X_AXIS][MAX_Y_AXIS]; //each array element records the visited grid

        Random rand = new Random(System.currentTimeMillis());
        Robot robot = new Robot();

        int initX = rand.nextInt(MAX_X_AXIS), initY = rand.nextInt(MAX_Y_AXIS);
        assertEquals("excepted right placement:", robot.hopOn(tableSurface,
                initX, initY, //put robot to a random position
                FacingDirection.values()[rand.nextInt(FacingDirection.values().length)]), //put a random facing direction
                true);
        mapStatus[initX][initY] = true;

        while (!this.checkAllPathVisited(mapStatus)) {
            int moveTimes = rand.nextInt(10); //move randomly steps towards current direction
            int nMove = 0;
            while (nMove++ < moveTimes) {
                boolean moved = robot.move(); //try to move
                if (!moved) {
                    System.out.println("@_@ STOPPING MOVING OUT OF THE TABLE, KEEP THE POSITION: "
                            + robot.toString()); //preventing to move out of edge warning message

                    break; //if reach the edge just quit
                }
                else {
                    mapStatus[robot.getPosX()][robot.getPosY()] = true; //set it as visited
                    System.out.println("^o^ MOVE: "+robot.toString());
                }
            }

            //randomly turning into another direction
            boolean turnLeft = rand.nextBoolean();
            if (turnLeft) {
                robot.turnLeft();
                System.out.println("<<< LEFT: "+robot.toString());
            }
            else {
                robot.turnRight();
                System.out.println(">>> RIGHT: "+robot.toString());
            }
        }

        robot.getOff();

        System.out.println("lol Congrats! All table covered!");
    }

    private boolean checkAllPathVisited(boolean[][] mapStatus) {
        for (int i = 0; i < mapStatus.length; ++i) {
            for (int j = 0; j < mapStatus.length; ++j) {
                if (!mapStatus[i][j]) {
                    return false;
                }
            }
        }

        return true;
    }
}