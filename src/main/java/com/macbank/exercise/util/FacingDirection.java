package com.macbank.exercise.util;

/**
 * Sequence is in clockwise
 */
public enum FacingDirection {
    NORTH,   //north 0
    EAST,   //east  1
    SOUTH,   //south 2
    WEST    //west  3
}
