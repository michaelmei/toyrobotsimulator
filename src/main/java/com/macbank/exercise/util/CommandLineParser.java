package com.macbank.exercise.util;

/*
> > . Create an application that can read in commands of the following form -
> > PLACE X,Y,F
> > MOVE
> > LEFT
> > RIGHT
> > REPORT
 */
public class CommandLineParser {
    public static void printCommandHints() {
        System.out.println("Command specification: ");
        System.out.println(" PLACE X,Y,F (where x,y are axises of the table surface x and y " +
                "and F is the Facing Direction: NORTH, EAST, SOUTH, WEST)");
        System.out.println(" MOVE ");
        System.out.println(" LEFT ");
        System.out.println(" RIGHT ");
        System.out.println(" REPORT ");
    }

    public static CommandResult parse(final String commandString) {
        try {
            CommandResult commandResult = new CommandResult();

            String[] args = commandString.trim().split(" ");
            Command command = Command.valueOf(args[0].trim());
            switch (command) {
                case LEFT:
                case MOVE:
                case RIGHT:
                case REPORT:
                    if (args.length == 1) {
                        commandResult.setCommand(command);
                        return commandResult;
                    }

                    break;
                case PLACE:
                    String param = args[1].trim();
                    String[] params = param.split(",");

                    if (params.length == 3) {
                        Integer posX = Integer.parseInt(params[0].trim());
                        Integer posY = Integer.parseInt(params[1].trim());
                        FacingDirection facingDirection = FacingDirection.valueOf(params[2].trim().toUpperCase());
                        commandResult.setCommand(command);
                        commandResult.setPosX(posX);
                        commandResult.setPosY(posY);
                        commandResult.setFacingDirection(facingDirection);
                        return commandResult;
                    }

                    break;
            }
        }
        catch (Exception e) {
            //we do not explicit report error here, just return null to let the caller to handle the exception
        }

        return null;
    }
}
