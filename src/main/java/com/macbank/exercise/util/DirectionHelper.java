package com.macbank.exercise.util;

public class DirectionHelper {
    public static FacingDirection left(FacingDirection currentFacingDirection) {
        int targetOrdinal = currentFacingDirection.ordinal() - 1;

        if (targetOrdinal < 0) {
            targetOrdinal = FacingDirection.values().length-1; //anti-clock wise rotation
        }

        return FacingDirection.values()[targetOrdinal];
    }

    public static FacingDirection right(FacingDirection currentFacingDirection) {
        int targetOrdinal = currentFacingDirection.ordinal() + 1;

        if (targetOrdinal > FacingDirection.values().length-1) {
            targetOrdinal = 0; //clock wise rotation
        }

        return FacingDirection.values()[targetOrdinal];
    }
}
