package com.macbank.exercise.util;

public enum Command {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT
}
