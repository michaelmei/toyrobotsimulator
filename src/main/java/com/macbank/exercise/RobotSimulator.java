package com.macbank.exercise;

/*
> > Toy Robot Simulator
> >
> > Description:
> > . The application is a simulation of a toy robot moving on a square tabletop, of dimensions 5 units x 5 units.
> > . There are no other obstructions on the table surface.
> > . The robot is free to roam around the surface of the table, but must be prevented from falling to destruction. Any movement
> > that would result in the robot falling from the table must be prevented, however further valid movement commands must still
> > be allowed.
*/

import com.macbank.exercise.util.Command;
import com.macbank.exercise.util.CommandLineParser;
import com.macbank.exercise.util.CommandResult;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RobotSimulator {
    private static TableSurface tableSurface;
    private static Robot robot;

    public static void main(String[] args) {
        tableSurface = TableSurface.getTableInstance(4,4); //5x5 grids
        robot = new Robot();

        String commandLine;

        System.out.println("Toy Robot Powered On, Enjoy! Type 'EXIT' to exit");
        CommandLineParser.printCommandHints();

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            while (null != (commandLine = br.readLine())) {
                final String command = commandLine.trim();
                if (command.trim().toUpperCase().equals("EXIT")) {
                    System.exit(0);
                }
                else {
                    robot.executeInstruction(tableSurface, command);
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
