package com.macbank.exercise;

/**
 *  5 X 5 table
 *   N
 * W   E
 *   S
 *
 *  MovingAxis
 *  4
 *  3
 *  2
 *  1
 *  0  1  2  3  4
 */
public class TableSurface {
    private static TableSurface tableSurface;

    private int dimensionX;
    private int dimensionY;

    private TableSurface(int dimensionX, int dimensionY) {
        this.dimensionX = dimensionX;
        this.dimensionY = dimensionY;
    }

    /**
     * To create a tableSurface instance
     * @param dimensionX start from 0, to control the table surface's max X value [0, dimensionX]
     * @param dimensionY start from 0, to control the table surface's max Y value [0, dimensionY]
     * @return
     */
    public static TableSurface getTableInstance(int dimensionX, int dimensionY) {
        if (null == tableSurface) {
            tableSurface = new TableSurface(dimensionX, dimensionY);
        }

        return tableSurface;
    }

    public boolean checkInRange(int posXToMove, int posYToMove) {
        if (posXToMove < 0 || posXToMove > dimensionX) {
            return false;
        }

        if (posYToMove < 0 || posYToMove > dimensionY) {
            return false;
        }

        return true;
    }
}
