package com.macbank.exercise;

import com.macbank.exercise.util.CommandLineParser;
import com.macbank.exercise.util.CommandResult;
import com.macbank.exercise.util.FacingDirection;
import com.macbank.exercise.util.DirectionHelper;

public class Robot {

    private int posX;
    private int posY;

    private FacingDirection facing;

    private TableSurface tableSurfaceAttached = null;

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public void executeInstruction(final TableSurface tableSurface, final String command) {
        CommandResult commandInstruction = CommandLineParser.parse(command);
        if (null == commandInstruction) {
            CommandLineParser.printCommandHints();
            return;
        }

        switch (commandInstruction.getCommand()) {
            case REPORT:
                report();
                break;
            case LEFT:
                turnLeft();
                break;
            case RIGHT:
                turnRight();
                break;
            case MOVE:
                move();
                break;
            case PLACE:
                hopOn(tableSurface,
                      commandInstruction.getPosX(), commandInstruction.getPosY(),
                      commandInstruction.getFacingDirection());
                break;
        }
    }

    public boolean hopOn(TableSurface tableSurface, int posX, int posY, FacingDirection facing) {
        if (tableSurface.checkInRange(posX, posY)) {
            //put the robot on the table surface successfully
            tableSurfaceAttached = tableSurface;

            this.posX = posX;
            this.posY = posY;
            this.facing = facing;
            return true;
        }
        else {
            this.getOff();
        }

        return false;
    }

    public void getOff() {
        tableSurfaceAttached = null;
    }

    public boolean move() {
        if (null == tableSurfaceAttached) {
            return false;
        }

        switch (facing) {
            case EAST:
                //+x
                if (tableSurfaceAttached.checkInRange(posX + 1, posY)) {
                    //do the real move
                    posX++;
                    return true;
                }
                break;
            case NORTH:
                //+y
                if (tableSurfaceAttached.checkInRange(posX, posY + 1)) {
                    //do the real move
                    posY++;
                    return true;
                }
                break;
            case WEST:
                //-x
                if (tableSurfaceAttached.checkInRange(posX - 1, posY)) {
                    //do the real move
                    posX--;
                    return true;
                }
                break;
            case SOUTH:
                //-y
                if (tableSurfaceAttached.checkInRange(posX, posY - 1)) {
                    //do the real move
                    posY--;
                    return true;
                }
                break;
        }

        return false;
    }

    public boolean turnLeft() {
        if (null == tableSurfaceAttached) {
            return false;
        }

        if (null != facing) {
           facing = DirectionHelper.left(facing);
           return true;
        }

        return false;
    }

    public boolean turnRight() {
        if (null == tableSurfaceAttached) {
            return false;
        }

        if (null != facing) {
            facing = DirectionHelper.right(facing);
            return true;
        }

        return false;
    }

    public void report() {
        if (null == tableSurfaceAttached) {
            System.out.println("Output: Not on any table");
            return;
        }

        System.out.println("Output: "+this.toString());
    }

    @Override
    public String toString() {
        return posX+","+posY+","+facing.toString();
    }
}
